using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public AudioMixer MainMixer;
    public AnimationCurve mainMenuMusicVolCurve;
    public Slider VMMSlider;
    float vMMSliderVol;
    bool muted = false;
    void Start()
    {
        SetMainMenuMusicVol(0.5f);
        VMMSlider.value = 0.5f;
    }

    public void SetMainMenuMusicVol(float mmMusicVol)
    {
        float mmvol = mainMenuMusicVolCurve.Evaluate(mmMusicVol);
        MainMixer.SetFloat("MainMenuMusicVol", mmvol);
    }

    public void MuteMainMenuMusic()
    {
        if (!muted)
        {
            MainMixer.SetFloat("MainMenuMusicVol", -80f);
            vMMSliderVol = VMMSlider.value;
            VMMSlider.value = -80f;
            muted = true;
        }
        else if (muted)
        {
            MainMixer.SetFloat("MainMenuMusicVol", vMMSliderVol);
            VMMSlider.value = vMMSliderVol;
            muted = false;
        }
    }
}
