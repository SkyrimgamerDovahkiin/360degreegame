using UnityEngine;

public class Enemy : MonoBehaviour
{
    public EnemyState patrolling;
    public EnemyState chase;
    public EnemyState jump;
    bool isRotated = false;
    public EnemyData data;

    void Start()
    {
        data.enemy = this;
        patrolling = GetComponent<PatrollingState>();
        patrolling.Init(data);

        if (data.enemyType == EnemyType.Clown)
        {
            chase = GetComponent<ChaseState>();
            jump = GetComponent<JumpState>();
            chase.Init(data);
            jump.Init(data);
        }

        data.currentState = patrolling;
        data.currentState.OnStateEnter();
    }

    void Update()
    {
        Debug.Log(data.currentState);
        data.currentState.OnUpdate();
        data.levelIdx = (int)(transform.position.y / 2.5f);
        data.SubtractedLevelIdx = data.levelIdx - data.PlayerMovement.levelIdx;

        if (Mathf.Sign(data.Speed) > 0f && !isRotated)
        {
            data.EnemyMesh.transform.rotation = Quaternion.Euler(
                data.EnemyMesh.transform.rotation.eulerAngles.x,
                data.EnemyMesh.transform.rotation.eulerAngles.y + 180f,
                data.EnemyMesh.transform.rotation.eulerAngles.z);
            isRotated = true;
        }

        else if (Mathf.Sign(data.Speed) < 0f && isRotated)
        {
            data.EnemyMesh.transform.rotation = Quaternion.Euler(
                data.EnemyMesh.transform.rotation.eulerAngles.x,
                data.EnemyMesh.transform.rotation.eulerAngles.y - 180f,
                data.EnemyMesh.transform.rotation.eulerAngles.z);
            isRotated = false;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(new Vector3(data.center.x, transform.position.y, data.center.z), data.Radius);
        float x = Mathf.Cos(data.inputX);
        float z = Mathf.Sin(data.inputX);
        Vector3 posOnCircle = new Vector3(x * data.Radius, transform.position.y, z * data.Radius);
        Gizmos.DrawWireSphere(posOnCircle, 1f);
    }

    public void SwitchState(EnemyState state)
    {
        data.lastState = data.currentState;
        data.currentState.OnStateExit();
        data.currentState = state;
        data.currentState.OnStateEnter();
    }

    void OnTriggerEnter(Collider other)
    {
        if (((1 << other.gameObject.layer) & data.layerMask) == 0 && data.currentState == data.enemy.patrolling)
        {
            data.Speed *= -1;
        }

        else if (other.gameObject.name == "JumpTrigger" && ((1 << other.gameObject.layer) & data.layerMask) == 0 && (data.currentState == data.enemy.chase || data.currentState == data.enemy.jump))
        {
            switch (other.GetComponent<TriggerType>().triggerType)
            {
                case (Trigger.Jump):
                    data.YPosition = transform.position.y;
                    data.TriggerType = Trigger.Jump;
                    data.enemy.SwitchState(data.enemy.jump);
                    break;
                case (Trigger.JumpGap):
                    data.YPosition = transform.position.y;
                    data.TriggerType = Trigger.JumpGap;
                    data.enemy.SwitchState(data.enemy.jump);
                    break;
                case (Trigger.JumpGapSpeed):
                    data.YPosition = transform.position.y;
                    data.TriggerType = Trigger.JumpGapSpeed;
                    data.enemy.SwitchState(data.enemy.jump);
                    break;
                case (Trigger.JumpSmall):
                    data.YPosition = transform.position.y;
                    data.TriggerType = Trigger.JumpSmall;
                    data.enemy.SwitchState(data.enemy.jump);
                    break;
                case (Trigger.JumpGapSmall):
                    data.YPosition = transform.position.y;
                    data.TriggerType = Trigger.JumpGapSmall;
                    data.enemy.SwitchState(data.enemy.jump);
                    break;
                case (Trigger.JumpBig):
                    data.YPosition = transform.position.y;
                    data.TriggerType = Trigger.JumpBig;
                    data.enemy.SwitchState(data.enemy.jump);
                    break;
                default:
                    break;
            }
        }

        if (other.CompareTag("BulletPlayer"))
        {
            gameObject.SetActive(false);
        }
    }

}
[System.Serializable]
public class EnemyData
{
    public Enemy enemy;
    [HideInInspector] public Vector3 center;
    [HideInInspector] public Vector3 newDir;
    [HideInInspector] public Vector3 NewBulletDir;
    [HideInInspector] public Vector3 velocity;
    [HideInInspector] public EnemyState currentState;
    [HideInInspector] public EnemyState lastState;
    public Animator ClownAnim;
    public Animation DuckAnim;
    public Animation MouseAnim;
    public AudioClip ClownClip;
    public AudioClip MouseAndDuckClip;
    public GameObject player;
    public GameObject bullet;
    public GameObject BulletPrefab;
    public CharacterController cc;
    public PlayerMovement PlayerMovement;
    public Trigger TriggerType;
    public EnemyType enemyType;
    public LayerMask layerMask;
    public Transform EnemyMesh;
    public Transform gunEnd;

    // ------------------- floats, ints, bools --------------------
    public float NextFire;
    public float detectRadius = 5f;
    public float Speed = 0.25f;
    public float BulletSpeed = 0.25f;
    public float Radius;
    [Range(0, 2 * Mathf.PI)]
    public float inputX;
    public float BulletInputX;
    public float curInputX;
    public float targetInputX;
    public float deltaInputX;
    public float vSpeed = 0f;
    public float Gravity = 9.81f;
    public float PlayerYPos;
    public float YPosition;
    public float JumpSpeed;
    public float JumpHeight;
    public float SmallJumpHeight;
    public float BigJumpHeight = 14f;
    public float FireRate = 0.25f;
    public float JumpSign;
    public float SpeedChange = 0.25f;
    public int levelIdx;
    public int SubtractedLevelIdx;
    public bool speedChanged = false;
}

public enum EnemyType { Clown, Duck, Mouse }