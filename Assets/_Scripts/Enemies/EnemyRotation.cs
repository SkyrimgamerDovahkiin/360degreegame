using UnityEngine;

public class EnemyRotation : MonoBehaviour
{
    Camera cam;
    Vector3 rotVector;

    void Start()
    {
        cam = Camera.main;
        rotVector = new Vector3(
            transform.rotation.eulerAngles.x,
            cam.transform.rotation.eulerAngles.y,
            transform.rotation.eulerAngles.z);
        transform.rotation = Quaternion.Euler(rotVector);
    }

    void Update()
    {
        rotVector = new Vector3(
            transform.rotation.eulerAngles.x,
            cam.transform.rotation.eulerAngles.y,
            transform.rotation.eulerAngles.z);
        transform.rotation = Quaternion.Euler(rotVector);
    }
}