using UnityEngine;

public class EnemyBulletCheckCollision : MonoBehaviour
{
    public LayerMask layerMask;
    Vector3 lastPos;

    void Update()
    {
        RaycastHit hit;
        Vector3 dir = lastPos - transform.position;

        if (lastPos != Vector3.zero)
        {
            if (Physics.Raycast(transform.position, dir, out hit, dir.magnitude, layerMask, QueryTriggerInteraction.Ignore))
            {
                if (hit.collider.CompareTag("Player") || hit.collider.CompareTag("Ground"))
                {
                    Destroy(this.gameObject);
                }
            }
            Debug.DrawRay(transform.position, dir, Color.red);
        }
        lastPos = transform.position;
    }
}
