using UnityEngine;

public class HandHelper : MonoBehaviour
{
    public bool PlayerHit = false;
    public Transform playerCatchEmpty;
    public Transform playerDestinationEmpty;
    
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHit = true;
        }
    }
}
