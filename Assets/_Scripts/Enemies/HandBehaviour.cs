using UnityEngine;

public class HandBehaviour : MonoBehaviour
{
    public Animator HandAnimator;
    public HandHelper HandHelper;
    bool canPlay = false;
    int startAnim = 0;

    void Update()
    {
        startAnim = Random.Range(0, 1000);

        if (canPlay && startAnim > 970)
        {
            HandAnimator.SetBool("CantMove", false);
            if (HandHelper.PlayerHit)
            {
                HandAnimator.SetBool("CanMove", true);
                HandAnimator.SetBool("CanMoveNormal", false);
                HandAnimator.SetBool("CanMoveForward", false);
                HandAnimator.SetBool("CanMoveBackward", true);
            }
            else if (!HandHelper.PlayerHit)
            {
                HandAnimator.SetBool("CanMove", false);
                HandAnimator.SetBool("CanMoveForward", false);
                HandAnimator.SetBool("CanMoveBackward", false);
                HandAnimator.SetBool("CanMoveNormal", true);
            }
        }

        else if (!canPlay && HandAnimator.GetCurrentAnimatorStateInfo(0).IsName("HandMoveForward"))
        {
            HandAnimator.SetBool("CantMove", false);
        }

        else if (!canPlay)
        {
            HandAnimator.SetBool("CanMove", false);
            HandAnimator.SetBool("CanMoveNormal", false);
            HandAnimator.SetBool("CanMoveForward", false);
            HandAnimator.SetBool("CanMoveBackward", false);
            HandAnimator.SetBool("CantMove", true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canPlay = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canPlay = false;
        }
    }
}
