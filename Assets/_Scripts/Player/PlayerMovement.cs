using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public GAME game;
    public float FireRate = 0.25f;
    public float jumpSpeed;
    public float JumpHeight = 1.0f;
    public float gravity = 9.81f;
    public float vSpeed;
    public float Radius = 3f;
    public float Speed = 40f;
    [Range(0, 2 * Mathf.PI)]
    public float inputX = 0f;
    public int levelIdx;
    public int AmmunitionUsed = 1;
    public Animator PlayerAnimator;
    public AudioClip JumpClip;
    public AudioClip ShootClip;
    public LayerMask layerMask;
    public Checkpoint Checkpoint;
    public Transform gunEnd;
    public GameObject playerMesh;
    public GameObject BulletPrefab;
    public InputActionAsset inputActions;
    InputAction MoveAction;
    InputAction FireAction;
    InputAction MoveRotateAction;
    InputAction JumpAction;
    InputAction RotateLeftAction;
    InputAction RotateRightAction;
    CharacterController cc;
    AudioSource source;
    Transform playerCatchEmpty;
    Transform playerDestinationEmpty;
    Vector3 velocity;
    Vector3 center;
    Vector3 newDir;
    CharSound characterSound;
    bool isCatched;
    bool candoublejump;
    bool canMove = true;
    bool isLeftRotating;
    bool isFacingRight = true;
    float NextFire;
    float step;

    void Awake()
    {
        var gameplayActionMap = inputActions.FindActionMap("Player");
        MoveAction = gameplayActionMap.FindAction("Move");
        FireAction = gameplayActionMap.FindAction("Fire");
        MoveRotateAction = gameplayActionMap.FindAction("MoveRotate");
        JumpAction = gameplayActionMap.FindAction("Jump");
        RotateLeftAction = gameplayActionMap.FindAction("RotateLeft");
        RotateRightAction = gameplayActionMap.FindAction("RotateRight");
        FireAction.Enable();
        MoveRotateAction.Enable();
        RotateLeftAction.Enable();
        RotateRightAction.Enable();
        characterSound = GetComponent<CharSound>();
        cc = GetComponent<CharacterController>();
        source = GetComponent<AudioSource>();
        Checkpoint = GetComponent<Checkpoint>();
        center = new Vector3();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(center, Radius);
        float x = Mathf.Cos(inputX);
        float z = Mathf.Sin(inputX);
        Vector3 posOnCircle = new Vector3(x * Radius, 0f, z * Radius);
        Gizmos.DrawWireSphere(posOnCircle, 1f);
    }

    void Update()
    {
        levelIdx = (int)(transform.position.y / 2.5f);

        step = 20f * Time.deltaTime;
        if (transform.position.y <= -6f)
        {
            Checkpoint.Reset();
        }
        if (isCatched)
        {
            var offset = playerDestinationEmpty.position - playerCatchEmpty.position;
            var offsetPlayer = playerCatchEmpty.position - transform.position;
            if (offset.magnitude > 0.1f)
            {
                if (offsetPlayer.magnitude > 0.1f)
                {
                    offsetPlayer = offsetPlayer.normalized * step;
                    cc.Move(offsetPlayer);
                }
            }
            else if (offset.magnitude < 0.1f)
            {
                Debug.Log("Player is killed!");
                isCatched = false;
                transform.parent = null;
                Checkpoint.Reset();
            }
            Debug.Log(offset.magnitude + " offset magnitude!");
        }

        if (canMove)
        {
            RotateLeftAction.performed += ctx => isLeftRotating = true;
            RotateRightAction.performed += ctx => isLeftRotating = false;
        }

        if (cc.isGrounded)
        {
            vSpeed = 0f;
        }

        // //PLAYER MESH ROTATING
        if (isLeftRotating)
        {
            playerMesh.transform.rotation = Quaternion.Euler(
                playerMesh.transform.rotation.eulerAngles.x,
                playerMesh.transform.rotation.eulerAngles.y + 180f,
                playerMesh.transform.rotation.eulerAngles.z);
        }

        if (FireAction.triggered && game.AmmunitionCounter > 0f && canMove)
        {
            if (Time.time > NextFire)
            {
                AudioSource.PlayClipAtPoint(ShootClip, transform.position);
                game.AmmunitionCounter -= AmmunitionUsed;
                NextFire = Time.time + FireRate;
                var bullet = Instantiate<GameObject>(BulletPrefab, gunEnd.position, gunEnd.rotation);
                bullet.GetComponent<Bullet>().Init(isFacingRight, inputX, Radius);
            }
        }

        Move();

        //JUMP
        if (JumpAction.triggered && cc.isGrounded && canMove)
        {
            PlayerAnimator.Play("Jump", 0);
            AudioSource.PlayClipAtPoint(JumpClip, transform.position);
            Jump();
            candoublejump = true;
        }
        else if (JumpAction.triggered && candoublejump)
        {
            //PlayerAnimator.Play("Jump", 0);
            PlayerAnimator.Play("Jump", -1, 0f);
            AudioSource.PlayClipAtPoint(JumpClip, transform.position);
            Jump();
            candoublejump = false;
        }

        Gravity();
    }

    public void Gravity()
    {
        if (isCatched) { return; }
        if (!cc.isGrounded)
        {
            vSpeed += gravity * Time.deltaTime;
            velocity += Vector3.down * vSpeed * Time.deltaTime;
        }
        if (!canMove)
        {
            cc.Move(velocity * Time.deltaTime);
        }
    }

    void Move()
    {
        if (!canMove || isCatched) { return; }
        Vector2 moveVector = MoveRotateAction.ReadValue<Vector2>();
        if (PlayerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Empty") && Mathf.Abs(moveVector.x) > 0)
        {
            PlayerAnimator.Play("Walk", 0);
        }
        if (Mathf.Abs(moveVector.x) <= 0.005f && PlayerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
        {
            PlayerAnimator.Play("Empty", 0);
        }
        float iX = moveVector.x * Time.deltaTime * 0.5f;
        characterSound.walkSpeed = Mathf.Abs(moveVector.x) * 0.5f;
        if (iX != 0f)
        {
            isFacingRight = iX > 0f;
        }
        inputX += iX;
        if (GetNewDir(inputX).sqrMagnitude > 0f)
        {
            Ray ray = new Ray(transform.position + Vector3.up, GetNewDir(inputX));
            if (Physics.Raycast(ray, 1f, layerMask, QueryTriggerInteraction.Ignore))
            {
                inputX -= iX;
            }
        }
        inputX = inputX % (2f * Mathf.PI);
        newDir = GetNewDir(inputX);
        cc.Move(newDir + velocity * Time.deltaTime);
    }

    Vector3 GetNewDir(float inputX)
    {
        float newX = Mathf.Cos(inputX);
        float newZ = Mathf.Sin(inputX);
        var xzPos = new Vector3(transform.position.x, 0f, transform.position.z);
        var newPos = new Vector3(newX * Radius, 0f, newZ * Radius);
        newDir = newPos - xzPos;
        return newDir;
    }

    void Jump()
    {
        velocity.y = 0f;
        if (!cc.isGrounded)
        {
            jumpSpeed = gravity * 0.04f + (JumpHeight + 4f);
        }
        else
        {
            jumpSpeed = gravity * 0.04f + JumpHeight;
        }
        velocity += Vector3.up * jumpSpeed;
    }
    void OnEnable()
    {
        MoveAction.Enable();
        JumpAction.Enable();
    }

    void OnDisable()
    {
        MoveAction.Disable();
        JumpAction.Disable();
    }

    // GODMODE
    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //if (hit.gameObject.CompareTag("Enemy") || hit.gameObject.CompareTag("BulletEnemy"))
        //{
        //    Checkpoint.Reset();
        //}
    }

    public void OnCollisionEnter(Collision other)
    {
        //if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("BulletEnemy"))
        //{
        //    Checkpoint.Reset();
        //}
    }


    void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("BulletEnemy"))
        //{
        //    Checkpoint.Reset();
        //}

        //else if (other.gameObject.CompareTag("EnemyHand"))
        //{
        //    playerDestinationEmpty = other.GetComponent<HandHelper>().playerDestinationEmpty;
        //    playerCatchEmpty = other.GetComponent<HandHelper>().playerCatchEmpty;
        //    transform.parent = playerCatchEmpty;
        //    isCatched = true;
        //}

        //else if (other.gameObject.CompareTag("Falltuer"))
        //{
        //    canMove = false;
        //}
    }

    void OnTriggerStay(Collider other)
    {
        //if (other.gameObject.CompareTag("Falltuer"))
        //{
        //    canMove = false;
        //}
    }

    void OnTriggerExit(Collider other)
    {
        //if (other.gameObject.CompareTag("Falltuer"))
        //{
        //    canMove = true;
        //}
    }
}