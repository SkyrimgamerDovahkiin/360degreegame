using UnityEngine;
using UnityEngine.InputSystem;

public class Checkpoint : MonoBehaviour
{
    public Transform CheckpointTransform;
    public Transform CheckpointEmpty;
    public InputActionAsset inputActions;
    InputAction CheckpointAction;
    CharacterController controller;
    PlayerMovement playerMovement;
    Transform startCheckpointTransform;
    Transform startCheckpointEmpty;
    float inputX;

    void Start()
    {
        var gameplayActionMap = inputActions.FindActionMap("Player");
        CheckpointAction = gameplayActionMap.FindAction("Checkpoint");
        CheckpointAction.Enable();
        controller = GetComponent<CharacterController>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    void Update()
    {
        if (CheckpointAction.triggered)
        {
            Reset();
        }
    }

    public void Reset()
    {
        playerMovement.inputX = inputX;
        controller.enabled = false;
        transform.position = CheckpointTransform.position;
        controller.enabled = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "CheckpointTrigger")
        {
            if (startCheckpointTransform == null)
            {
                startCheckpointTransform = other.transform;
                startCheckpointEmpty = other.transform.parent;
            }

            else if (startCheckpointTransform != null)
            {
                startCheckpointEmpty.gameObject.SetActive(false);
            }
            inputX = playerMovement.inputX;
            CheckpointTransform = other.transform;
            CheckpointEmpty = other.transform.parent;
        }

        // if (other.gameObject.name == "CheckpointTrigger")
        // {
        // }
    }
}