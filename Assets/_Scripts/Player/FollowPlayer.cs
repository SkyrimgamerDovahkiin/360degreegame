using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform playerMeshT;
    public float offset = 0f;
    Vector3 center = new Vector3();
    Vector3 playerMeshRotVector;
    Transform playerT;
    float yOffset;
    float distance;

    void Start()
    {
        playerT = GameObject.Find("Player").transform;
        yOffset = transform.position.y;
        distance = Vector3.Distance(playerT.position, transform.position);
        playerMeshRotVector = new Vector3(playerMeshT.rotation.eulerAngles.x,
            gameObject.transform.rotation.eulerAngles.y,
            playerMeshT.rotation.eulerAngles.z);

        playerMeshT.rotation = Quaternion.Euler(playerMeshRotVector);
    }

    void Update()
    {
        playerMeshRotVector = new Vector3(playerMeshT.rotation.eulerAngles.x,
            gameObject.transform.rotation.eulerAngles.y,
            playerMeshT.rotation.eulerAngles.z);
            
        playerMeshT.rotation = Quaternion.Euler(playerMeshRotVector);
        Vector3 playerXZpos = new Vector3(playerT.position.x, 0f, playerT.position.z);
        Vector3 dir = playerXZpos - center;
        dir.Normalize();
        Vector3 xzPos = center + dir * distance;
        transform.position = new Vector3(xzPos.x, playerT.position.y + offset, xzPos.z);
        transform.LookAt(playerT);
    }
}