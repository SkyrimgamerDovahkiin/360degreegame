using UnityEngine;

public class TriggerType : MonoBehaviour
{
    public Trigger triggerType;
}

public enum Trigger
{
    Jump, Stay, JumpGap, Patrolling, JumpSmall, JumpBig, JumpGapSmall, JumpGapSpeed
}