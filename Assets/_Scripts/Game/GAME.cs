using UnityEngine;
using TMPro;

public class GAME : MonoBehaviour
{
    public int ScoreCounter;
    public int AmmunitionCounter;
    public TextMeshProUGUI ScoreCount;
    public TextMeshProUGUI AmmunitionCount;
    string ScoreCounterText;
    string AmmunitionCounterText;
    int lastScore;
    int lastAmmo;

    public void Start()
    {
        Application.targetFrameRate = 60;
    }

    void Update()
    {
        ScoreCounterText = ScoreCounter.ToString();
        for (int i = 0; i < ScoreCounterText.Length; i++)
        {
            if (lastScore != ScoreCounter)
            {
                if (ScoreCounter > 0 && ScoreCounter < 100)
                {
                    ScoreCount.text = "<sprite=" + (ScoreCounterText.Substring(0, 1)) + ">" + "<sprite=" + (ScoreCounterText.Substring(1)) + ">";
                }
                else if (ScoreCounter > 99 && ScoreCounter < 1000)
                {
                    ScoreCount.text = "<sprite=" + (ScoreCounterText.Substring(0, 1)) + ">" + "<sprite=" + (ScoreCounterText.Substring(1, 1)) + ">" + "<sprite=" + (ScoreCounterText.Substring(2)) + ">";
                }
                else if (ScoreCounter > 999 && ScoreCounter < 10000)
                {
                    ScoreCount.text = "<sprite=" + (ScoreCounterText.Substring(0, 1)) + ">" + "<sprite=" + (ScoreCounterText.Substring(1, 1)) + ">" + "<sprite=" + (ScoreCounterText.Substring(2, 1)) + ">" + "<sprite=" + (ScoreCounterText.Substring(3)) + ">";
                }
            }
        }
        lastScore = ScoreCounter;

        AmmunitionCounterText = AmmunitionCounter.ToString();
        for (int i = 0; i < AmmunitionCounterText.Length; i++)
        {
            if (lastAmmo != AmmunitionCounter)
            {
                if (AmmunitionCounter > 9 && AmmunitionCounter < 100)
                {
                    AmmunitionCount.text = "<sprite=" + (AmmunitionCounterText.Substring(0, 1)) + ">" + "<sprite=" + (AmmunitionCounterText.Substring(1)) + ">";
                }
                else if (AmmunitionCounter > 99 && AmmunitionCounter < 1000)
                {
                    AmmunitionCount.text = "<sprite=" + (AmmunitionCounterText.Substring(0, 1)) + ">" + "<sprite=" + (AmmunitionCounterText.Substring(1, 1)) + ">" + "<sprite=" + (AmmunitionCounterText.Substring(2)) + ">";
                }
                else if (AmmunitionCounter > 999 && AmmunitionCounter < 10000)
                {
                    AmmunitionCount.text = "<sprite=" + (AmmunitionCounterText.Substring(0, 1)) + ">" + "<sprite=" + (AmmunitionCounterText.Substring(1, 1)) + ">" + "<sprite=" + (AmmunitionCounterText.Substring(2, 1)) + ">" + "<sprite=" + (AmmunitionCounterText.Substring(3)) + ">";
                }
                else if (AmmunitionCounter < 10)
                {
                    AmmunitionCount.text = "<sprite=" + (AmmunitionCounterText.Substring(0, 1)) + ">";
                }
            }
        }
        lastAmmo = AmmunitionCounter;
    }
}
