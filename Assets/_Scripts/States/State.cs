using UnityEngine;

public abstract class State : MonoBehaviour
{
    public virtual void OnStateEnter()
    {
    }
    public virtual void OnStateExit()
    {
    }
    public virtual void OnUpdate()
    {
    }
}