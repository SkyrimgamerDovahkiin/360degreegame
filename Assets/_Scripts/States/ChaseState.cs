using UnityEngine;

public class ChaseState : EnemyState
{
    bool stayTriggered = false;
    bool isFacingRight;

    public override void OnStateEnter() {}

    public override void OnStateExit() {}

    public override void OnUpdate()
    {
        if (stayTriggered)
        {
            stayTriggered = false;
        }

        if (Vector3.Distance(data.player.transform.position, transform.position) > data.detectRadius + 2)
        {
            data.enemy.SwitchState(data.enemy.patrolling);
        }

        // Enemy schießt, wenn Player auf gleicher Ebene
        if (data.SubtractedLevelIdx == 0)
        {
            if (Time.time > data.NextFire)
            {
                data.NextFire = Time.time + data.FireRate;
                data.bullet = Instantiate<GameObject>(data.BulletPrefab, data.gunEnd.position, data.gunEnd.rotation);
                data.bullet.GetComponent<Bullet>().Init(isFacingRight, data.inputX, data.Radius);
            }
        }

        Move();
        Gravity();
    }

    void Move()
    {
        float iX = data.Speed * Time.deltaTime;
        if (iX != 0f)
        {
            isFacingRight = iX > 0f;
        }
        data.inputX += iX;
        data.inputX = data.inputX % (2f * Mathf.PI);
        data.curInputX = ((data.inputX + Mathf.PI) % (2f * Mathf.PI)) - Mathf.PI;
        data.newDir = GetNewDir(data.inputX);
        data.targetInputX = data.PlayerMovement.inputX;
        data.deltaInputX = ((data.targetInputX - data.curInputX + Mathf.PI) % (2f * Mathf.PI)) - Mathf.PI;
        float sign = Mathf.Sign(data.deltaInputX);
        data.cc.Move((data.newDir) + data.velocity);

        data.Speed = Mathf.Abs(data.Speed) * sign;
        data.JumpSign = sign;
    }

    public void Gravity()
    {
        if (!data.cc.isGrounded)
        {
            data.vSpeed += data.Gravity * Time.deltaTime;
            data.velocity += Vector3.down * data.vSpeed * Time.deltaTime;
        }
    }

    Vector3 GetNewDir(float inputX)
    {
        float newX = Mathf.Cos(data.inputX);
        float newZ = Mathf.Sin(data.inputX);
        var xzPos = new Vector3(transform.position.x, 0f, transform.position.z);
        var newPos = new Vector3(newX * data.Radius, 0f, newZ * data.Radius);
        data.newDir = newPos - xzPos;
        return data.newDir;
    }
}
