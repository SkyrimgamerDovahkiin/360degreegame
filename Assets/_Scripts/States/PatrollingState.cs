using UnityEngine;

public class PatrollingState : EnemyState
{
    public override void OnStateEnter()
    {
        data.center = new Vector3();


        if (data.enemyType == EnemyType.Clown)
        {
            data.ClownAnim.Play("WalkHead", 0);
            data.ClownAnim.Play("WalkSpring", 1);
            data.ClownAnim.Play("WalkBox", 2);
        }

        else if (data.enemyType == EnemyType.Duck)
        {
            data.DuckAnim["DuckRightWheel"].layer = 2;
            data.DuckAnim["DuckSaw"].layer = 3;
            data.DuckAnim.Play("DuckLeftWheel");
            data.DuckAnim.Play("DuckRightWheel");
            data.DuckAnim.Play("DuckSaw");
        }

        else if (data.enemyType == EnemyType.Mouse)
        {
            data.MouseAnim["MouseLeftWheel1"].layer = 4;
            data.MouseAnim["MouseLeftWheel2"].layer = 5;
            data.MouseAnim["MouseRightWheel1"].layer = 6;
            data.MouseAnim["MouseRightWheel2"].layer = 7;
            data.MouseAnim.Play("MouseLeftWheel1");
            data.MouseAnim.Play("MouseLeftWheel2");
            data.MouseAnim.Play("MouseRightWheel1");
            data.MouseAnim.Play("MouseRightWheel2");
        }
        // Teleport();
    }

    public override void OnStateExit() {}

    public override void OnUpdate()
    {
        if (Vector3.Distance(data.player.transform.position, transform.position) < data.detectRadius && data.enemyType == EnemyType.Clown)
        {
            data.enemy.SwitchState(data.enemy.chase);
        }
        Move();
        Gravity();
    }

    void Move()
    {
        float iX = data.Speed * Time.deltaTime;
        data.inputX += iX;
        data.inputX = data.inputX % (2f * Mathf.PI);
        data.newDir = GetNewDir(data.inputX);
        data.cc.Move((data.newDir) + data.velocity);
    }

    // void Teleport()
    // {
    //     data.inputX = data.inputX % (2f * Mathf.PI);
    //     data.newDir = GetNewDir(data.inputX);
    //     data.cc.Move((data.newDir) + data.velocity);
    // }

    public void Gravity()
    {
        if (!data.cc.isGrounded)
        {
            data.vSpeed += data.Gravity * Time.deltaTime;
            data.velocity += Vector3.down * data.vSpeed * Time.deltaTime;
        }
    }

    Vector3 GetNewDir(float inputX)
    {
        float newX = Mathf.Cos(data.inputX);
        float newZ = Mathf.Sin(data.inputX);
        var xzPos = new Vector3(transform.position.x, 0f, transform.position.z);
        var newPos = new Vector3(newX * data.Radius, 0f, newZ * data.Radius);
        data.newDir = newPos - xzPos;
        return data.newDir;
    }

    // private void OnTriggerEnter(Collider other)
    // {
    // }
}
