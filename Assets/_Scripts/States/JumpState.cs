using UnityEngine;

public class JumpState : EnemyState
{
    float lastInputX;
    bool isJump = false;

    public override void OnStateEnter() { isJump = false; }

    public override void OnStateExit() {}

    public override void OnUpdate()
    {
        if (data.velocity.y != 0f) { Move(); }
        Gravity();

        if (data.cc.isGrounded) { data.vSpeed = 0f; }

        if (data.SubtractedLevelIdx < 0 && !isJump)
        {
            switch (data.TriggerType)
            {
                case (Trigger.Jump):
                    Jump();
                    isJump = true;
                    break;
                case (Trigger.JumpGap):
                    Jump();
                    isJump = true;
                    break;
                case (Trigger.JumpGapSpeed):
                    if (!data.speedChanged)
                    {
                        data.Speed = data.Speed + (data.SpeedChange * data.JumpSign);
                        data.speedChanged = true;
                    }
                    Jump();
                    isJump = true;
                    break;
                case (Trigger.JumpSmall):
                    JumpSmall();
                    isJump = true;
                    break;
                case (Trigger.JumpGapSmall):
                    JumpSmall();
                    isJump = true;
                    break;
                case (Trigger.JumpBig):
                    if (!data.speedChanged)
                    {
                        data.Speed = data.Speed + (data.SpeedChange * data.JumpSign);
                        data.speedChanged = true;
                    }
                    JumpBig();
                    isJump = true;
                    break;
                default:
                    break;
            }
        }

        else if (data.SubtractedLevelIdx > 0 && !isJump)
        {
            switch (data.TriggerType)
            {
                case (Trigger.JumpGapSpeed):
                    if (!data.speedChanged)
                    {
                        data.Speed = data.Speed + (data.SpeedChange * data.JumpSign);
                        data.speedChanged = true;
                    }
                    Jump();
                    isJump = true;
                    break;
                case (Trigger.JumpBig):
                    if (!data.speedChanged)
                    {
                        data.Speed = data.Speed + (data.SpeedChange * data.JumpSign);
                        data.speedChanged = true;
                    }
                    Jump();
                    isJump = true;
                    break;
                default:
                    break;
            }
        }

        else if (data.SubtractedLevelIdx == 0 && !isJump)
        {
            switch (data.TriggerType)
            {
                case (Trigger.JumpGap):
                    Jump();
                    isJump = true;
                    break;
                case (Trigger.JumpGapSpeed):
                    if (!data.speedChanged)
                    {
                        data.Speed = data.Speed + (data.SpeedChange * data.JumpSign);
                        data.speedChanged = true;
                    }
                    Jump();
                    isJump = true;
                    break;
                case (Trigger.JumpSmall):
                    JumpSmall();
                    isJump = true;
                    break;
                case (Trigger.JumpGapSmall):
                    JumpSmall();
                    isJump = true;
                    break;
                case (Trigger.JumpBig):
                    if (!data.speedChanged)
                    {
                        data.Speed = data.Speed + (data.SpeedChange * data.JumpSign);
                        data.speedChanged = true;
                    }
                    Jump();
                    isJump = true;
                    break;
                default:
                    break;
            }
        }

        if ((data.cc.isGrounded && data.velocity.y <= 0f))
        {
            data.enemy.SwitchState(data.enemy.chase);
        }
    }

    void Move()
    {
        lastInputX = data.inputX;
        float iX = data.Speed * Time.deltaTime;
        data.inputX += iX;
        data.inputX = data.inputX % (2f * Mathf.PI);
        data.newDir = GetNewDir(data.inputX);
        data.cc.Move(data.newDir + (data.velocity * Time.deltaTime));
    }

    public void Gravity()
    {
        if (!data.cc.isGrounded)
        {
            data.vSpeed += data.Gravity * Time.deltaTime;
            data.velocity += Vector3.down * data.vSpeed * Time.deltaTime;
        }
    }

    void Jump()
    {
        data.velocity.y = 0f;
        data.JumpSpeed = data.Gravity * 0.04f + data.JumpHeight;
        data.velocity += Vector3.up * data.JumpSpeed;
    }


    void JumpSmall()
    {
        data.velocity.y = 0f;
        data.JumpSpeed = data.Gravity * 0.04f + data.SmallJumpHeight;
        data.velocity += Vector3.up * data.JumpSpeed;
    }

    void JumpBig()
    {
        data.velocity.y = 0f;
        data.JumpSpeed = data.Gravity * 0.04f + data.BigJumpHeight;
        data.velocity += Vector3.up * data.JumpSpeed;
    }

    Vector3 GetNewDir(float inputX)
    {
        float newX = Mathf.Cos(data.inputX);
        float newZ = Mathf.Sin(data.inputX);
        var xzPos = new Vector3(transform.position.x, 0f, transform.position.z);
        var newPos = new Vector3(newX * data.Radius, 0f, newZ * data.Radius);
        data.newDir = newPos - xzPos;
        return data.newDir;
    }
}