using UnityEngine;

[System.Serializable]
public class Player_Data
{
    public float[] position;
    public float inputX;
    public int playerScore;
    public int playerAmmo;
    public int[] pickups;

    public Player_Data(Transform playerT, PlayerMovement playerMovement, GAME game)
    {
        inputX = playerMovement.inputX;
        playerScore = game.ScoreCounter;
        playerAmmo = game.AmmunitionCounter;
        position = new float[3];
        position[0] = playerT.position.x;
        position[1] = playerT.position.y;
        position[2] = playerT.position.z;
    }
}
