using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class BackToMenu : MonoBehaviour
{
    public CharacterController controller;
    public InputActionAsset inputActions;
    InputAction Back_MenuAction;
    public GameObject Esc_Menu;
    public bool menuAktiv = false;

    public void Start()
    {
        var gameplayActionMap = inputActions.FindActionMap("Player");
        Back_MenuAction = gameplayActionMap.FindAction("Back_Menu");
        Back_MenuAction.Enable();
        Esc_Menu.SetActive(false);
    }
    public void Update()
    {
        if (Back_MenuAction.triggered && !menuAktiv)
        {
            menuAktiv = true;
            Esc_Menu.SetActive(true);
            Time.timeScale = 0f;
        }

        else if (Back_MenuAction.triggered && menuAktiv)
        {
            menuAktiv = false;
            Esc_Menu.SetActive(false);
            Time.timeScale = 1f;
        }
    }
    public void MainMenu()
    {
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Main Menu");
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
