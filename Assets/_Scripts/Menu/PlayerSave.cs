using UnityEngine;

public class PlayerSave : MonoBehaviour
{
    public CharacterController controller;
    public Transform player;
    public PlayerMovement playerMovement;
    public GAME game;

    public void savePlayer()
    {
        SaveSystem.SavePlayer(player, playerMovement, game);
    }

    public void LoadPlayer()
    {
        Player_Data data = SaveSystem.LoadPlayer();
        playerMovement.inputX = data.inputX;
        game.ScoreCounter = data.playerScore;
        game.AmmunitionCounter = data.playerAmmo;
        controller.enabled = false;
        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        transform.position = position;
        controller.enabled = true;
    }
}
