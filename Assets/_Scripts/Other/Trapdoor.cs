using UnityEngine;

public class Trapdoor : MonoBehaviour
{
    public Animation animLeftDoor;
    public Animation animRightDoor;
    public Collider MeshCollider;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            animLeftDoor.Play();
            animRightDoor.Play();
            MeshCollider.enabled = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        MeshCollider.enabled = true;
    }
}
