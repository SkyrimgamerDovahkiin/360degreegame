using UnityEngine;
using UnityEngine.UI;

public class VolumeSliderImageChange : MonoBehaviour
{
    // public Image VolumeOff;
    // public Image VolumeLow;
    // public Image VolumeMiddle;
    // public Image VolumeHigh;
    public Image[] VolumeImages = new Image[4];
    Slider slider;

    void Start()
    {
        slider = GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (slider.value == 0f)
        {
            foreach (Image image in VolumeImages)
            {
                image.enabled = false;
            }
            VolumeImages[0].enabled = true;
        }
        else if (slider.value > 0f && slider.value <= 0.4f)
        {
            foreach (Image image in VolumeImages)
            {
                image.enabled = false;
            }
            VolumeImages[1].enabled = true;
        }
        else if (slider.value > 0.4f && slider.value <= 0.8f)
        {
            foreach (Image image in VolumeImages)
            {
                image.enabled = false;
            }
            VolumeImages[2].enabled = true;
        }
        else if (slider.value > 0.8f)
        {
            foreach (Image image in VolumeImages)
            {
                image.enabled = false;
            }
            VolumeImages[3].enabled = true;
        }
    }
}
