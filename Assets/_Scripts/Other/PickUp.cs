using UnityEngine;

public class PickUp : MonoBehaviour
{
    public GAME game;
    public AudioClip pickupClip;
    public int score;
    public int ammunition = 1;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (gameObject.CompareTag("PickupScore"))
            {
                AudioSource.PlayClipAtPoint(pickupClip, transform.position);
                game.ScoreCounter += score;
            }
            if (gameObject.CompareTag("PickupAmmunition"))
            {
                AudioSource.PlayClipAtPoint(pickupClip, transform.position);
                game.AmmunitionCounter += ammunition;
            }
            GameObject.Destroy(gameObject);
        }
    }
}