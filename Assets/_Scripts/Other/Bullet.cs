using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float BulletSpeed = 1f;
    public float BulletSpawnOffset = 0.1f;
    public float BulletInputX = 0f;
    public float Timer;
    public float BulletLifetime = 5f;
    float sign;
    float radius;
    bool isFacingRight;
    Vector3 NewBulletDir;

    public void Init(bool isFacingRight, float inputX, float radius)
    {
        this.isFacingRight = isFacingRight;
        this.radius = radius;
        sign = isFacingRight ? 1f : -1f;
        BulletInputX = inputX + (sign * BulletSpawnOffset);
    }
    void Update()
    {
        Timer += Time.deltaTime;
        float iX = BulletSpeed * Time.deltaTime * sign;
        BulletInputX += iX;
        BulletInputX = BulletInputX % (2f * Mathf.PI);

        NewBulletDir = GetNewBulletDir(BulletInputX);
        transform.position += NewBulletDir;


        if (Timer >= BulletLifetime)
        {
            Timer = 0f;
            Destroy(this.gameObject);
        }
    }

    Vector3 GetNewBulletDir(float inputX)
    {
        float newX = Mathf.Cos(inputX);
        float newZ = Mathf.Sin(inputX);
        var xzPos = new Vector3(transform.position.x, 0f, transform.position.z);
        var newPos = new Vector3(newX * radius, 0f, newZ * radius);
        NewBulletDir = newPos - xzPos;
        return NewBulletDir;
    }
}
