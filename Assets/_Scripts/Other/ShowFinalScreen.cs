using UnityEngine;
using UnityEngine.UI;

public class ShowFinalScreen : MonoBehaviour
{
    [SerializeField] Image finalScreen;
    void OnTriggerEnter(Collider other)
    {
        finalScreen.enabled = true;
    }
}
